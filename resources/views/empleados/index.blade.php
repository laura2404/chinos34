<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Lista de Empleados</h1>
    <table>
    <thead>
   <tr>
   <th>Nombre y Apellidos</th>
   <th>Cargo</th>
   <!--<th>Fecha de nacimiento</th>-->
   <!--<th>Fecha contratación</th>-->
   <!--<th>Dirección</th>-->
  <!-- <th>Ciudad</th>-->
   <th>Email</th>
  <!--<th>Jefe inmediato</th>-->
  <th class="text-danger">Ver detalles</th>
   </tr>
    </thead>
   <tbody>
   @foreach($empleados as $empleado)
   <td><stron class="text-danger">{{ $empleado ->FirstName}}{{$empleado->LastName }}</strong></td>
   <td>{{ $empleado ->Title}}</td>
   <!--<td>{{ $empleado ->BirthDate}}</td>
   <td>{{ $empleado ->HireDate}}</td>
   <td>{{ $empleado ->Adress}}</td>
   <td>{{ $empleado ->City}}</td>-->
   <td>{{ $empleado ->Email}}</td>
   <!--<td>{{ $empleado ->ReportsTo}}</td>-->
   <td><a class="btn btn-prymary" href="{{ url('empleados/'.$empleado->EmployeeId)}}">Ver información</a></td>

   @endforeach
   </tbody>
    </table>
</body>
</html>