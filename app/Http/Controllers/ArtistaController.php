<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use  App\Artista;
use Illuminate\Support\Facades\validator;

class ArtistaController extends Controller
{
    //Accion: Metodo en un controlador debe estar asociado en un
    //ruta
    //La programación en un caso de uso 
    
    public function index(){
        //obtener datos utilizar el modelo
        $listado_artistas =Artista:: paginate(5);
        
        //presentar la vista con los artistas de la base de datos 
       return view('artistas.index')->with("artistas", $listado_artistas);
    }
    //muestra el formulario de creación del artista
    public function create(){
       return view('artistas.new');
    }

    //captura los datos desde el cliente (formulario)
    public function store(Request $request){
     
     //validación   
    //reglas de validación
    //regla 1 establecer las reglas 
    $reglas=[
         "nombre_artista" => ['required','alpha', 'unique:artist,Name']
     ];
     //regla 2 crear el objeto validador
 
    $validador = validator::make($request->all(),$reglas);
    
    //validacion paso 3:validar y establecer acciones
  if(  $validador->fails())
 {
     //acciones cuando la validación falla
      return redirect  ('artistas/create')->withErrors($validador);
    }
    
    //guardar el artista utilizando el modelo 
    $a = new Artista();
    $a -> Name= $request->nombre_artista;
    $a -> save();
     
    //mensaje
    //redireccionamiento a la ruta que muestra la vista
    //with ::crea un flash  session::variable session para un solo request
     // 
    return redirect('artistas/create')
    ->with ("exito", "Artista registrado correctamente") 
    ->with ("nombre_artista", $a->Name);
}

}
