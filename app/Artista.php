<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Artista extends Model
{
    

    protected $table = "artists";
    protected $primaryKey= "AristIId";
    public $timestamps= false;
    
    //relación artista album
    //metodo albumes se va a devolver los albumes del artista

    public function albumes(){
        return $this->hasMany('App\Album', 'ArtistId' );
    }

}
