<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empleados extends Model
{

    protected $table= "employees";
    protected $primayKey= "EmployeeId";
    public   $timestamps = false;

    public function jefe_directo(){
        return $this-> belongsTo('App\Empleados','ReportsTo');
    }
}
