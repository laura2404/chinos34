<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//mi primera ruta 
Route::get('hola',function(){
    echo "hola estoy en laravel";
});
/*
Route::get('arreglo',function(){
    //crear un arreglo estudiantes
    $estudiantes=["JE" => "Jerónimo", 
                  "LA" => "Laura",
                  "PA" => "Paola" , 
                  "DA" =>"David"];
    //recorrer un arreglo
    //var_dump($estudiantes);
    //recorrer un arreglo
    foreach($estudiantes as $key => $value){ 
    echo "$value tiene indice $key <hr />";
    }
});
*/
/*Route::get('paises',function(){
    //crear un arreglo con información de paises
 
    $paises=[
        "Colombia" =>[
            "capital"   => "Bogotá",
            "moneda"    => "peso",
            "poblacion" => 50.732
        ],
        "Ecuador" =>[
            "capital"   => "Quito",
            "moneda"    => "Dolar",
            "poblacion" => 17.517
        ],
        "Brazil" =>[
            "capital"   => "Brasilia",
            "moneda"    => "Real",
            "poblacion" => 212.216
        ],
        "Bolivia" =>[
            "capital"   => "La paz",
            "moneda"    => "Boliviano",
            "poblacion" => 16.613
        ] 
    ];
 */
    
    //mostrar una vista 
  //  return view('paises')->with("paises", $paises) ;


 //ruta de controlador
   Route:: get('artistas', 'ArtistaController@index' );
   Route:: get('artistas/create', 'ArtistaController@create');
   Route:: post('artistas/store', 'ArtistaController@store' );

   //Rutas resource
   Route:: resource('empleados','Empleados');
 